﻿using System.Text;

namespace TrapezeCalculator
{
    public class Calculate2DFigureButton : IDisposable
    {
        private readonly CoordinatesTable2D _coordinatesTable;
        private readonly Results _results;

        public Calculate2DFigureButton(CoordinatesTable2D coordinatesTable, Results results)
        {
            _coordinatesTable = coordinatesTable;
            _results = results;

            Button = CalculatorForm.CreateButton("Рассчитать");

            Button.Click += Calculate;
        }

        public Button Button { get; }

        public void Dispose()
        {
            _coordinatesTable.Dispose();
            Button.Dispose();
        }

        private void Calculate(object? sender, EventArgs e)
        {
            IReadOnlyList<PointF>? coordinates = _coordinatesTable.Coordinates;

            if (coordinates == null)
            {
                _results.ShowError("При вычислении возникла ошибка!");
                return;
            }

            _results.ShowResults(coordinates);
        }
    }
}

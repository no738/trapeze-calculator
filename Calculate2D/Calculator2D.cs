﻿using System.Text;

namespace TrapezeCalculator
{
    public static class Calculator2D
    {
        public static float GetArea(this IReadOnlyList<PointF> coordinates)
        {
            float area = 0;

            for (var i = 0; i < coordinates.Count; i++)
            {
                int j = (i + 1) % coordinates.Count;
                PointF a = coordinates[i], b = coordinates[j];

                area += a.X * b.Y - a.Y * b.X;
            }

            area = MathF.Abs(area) / 2;

            return area;
        }

        public static float GetPerimeter(this IReadOnlyList<PointF> coordinates)
        {
            float perimeter = 0;

            PointF a = coordinates[^1], b = coordinates[0];
            perimeter += GetSideLength((a, b));

            for (var i = 0; i < coordinates.Count - 1; i++)
            {
                a = coordinates[i]; 
                b = coordinates[i + 1];

                perimeter += GetSideLength((a, b));
            }

            return perimeter;
        }

        public static string GetSidesLengthText(this IReadOnlyList<PointF> coordinates)
        {
            var sidesLength = new StringBuilder();

            string currentSideName;
            PointF a, b;

            for (var i = 0; i < coordinates.Count - 1; i++)
            {
                currentSideName = CalculatorForm.DotLabelNames[i] + CalculatorForm.DotLabelNames[i + 1];

                a = coordinates[i];
                b = coordinates[i + 1];
                sidesLength.Append($"{currentSideName}) {MathF.Round(GetSideLength((a, b)), 2)}\n");
            }

            currentSideName = CalculatorForm.DotLabelNames[^1] + CalculatorForm.DotLabelNames[0];

            a = coordinates[^1];
            b = coordinates[0];
            sidesLength.Append($"{currentSideName}) {MathF.Round(GetSideLength((a, b)), 2)}\n");

            return sidesLength.ToString();
        }

        public static float GetSideLength((PointF A, PointF B) vector)
        {
            float widthDifference = vector.B.X - vector.A.X;
            float heightDifference = vector.B.Y - vector.A.Y;
            float squaredDifferencesSum = MathF.Pow(widthDifference, 2) + MathF.Pow(heightDifference, 2);

            return MathF.Sqrt(squaredDifferencesSum);
        }
    }
}

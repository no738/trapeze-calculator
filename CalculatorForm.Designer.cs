﻿namespace TrapezeCalculator
{
    partial class CalculatorForm
    {
        private const string FormName = "Расчёт трапеции";
        private System.ComponentModel.IContainer _components = null;
        private CoordinatesTable2D _coordinatesTable;
        private Calculate2DFigureButton _calculateButton;

        private static Size _defaultTextBoxSize = new Size(70, 20);

        private static string[] _dotLabelsNames = { "A", "B", "C", "D" };
        private Results _output;

        public static string[] DotLabelNames => _dotLabelsNames;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (_components != null))
            {
                _components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            Name = FormName;
            Text = FormName;

            CreateCoordinatesTable();

            CreateResultsBlock();

            CreateCalculateButton();

            SuspendLayout();

            ConfigureSizes();

            ResumeLayout(true);
        }

        private void CreateCoordinatesTable()
        {
            var coordinatesTablePosition = new Point(20, 15);
            _coordinatesTable = new CoordinatesTable2D(_dotLabelsNames, coordinatesTablePosition);

            foreach (Control contorol in _coordinatesTable.Controls)
            {
                Controls.Add(contorol);
            }
        }

        private void CreateCalculateButton()
        {
            _calculateButton = new Calculate2DFigureButton(_coordinatesTable, _output);
            _calculateButton.Button.Location = GetCenterPositionRelativeTable(_coordinatesTable, _calculateButton.Button);

            Controls.Add(_calculateButton.Button);
        }

        private void CreateResultsBlock()
        {
            var blockPosition = new Point(200, 15);
            _output = new Results(blockPosition, out Control resultsBlock);

            Controls.Add(resultsBlock);
        }

        private Point GetCenterPositionRelativeTable(DataTable dataTable, in Button button)
        {
            TextBox firstTextBox = _coordinatesTable.DataBoxes[0];
            TextBox lastTextBox = _coordinatesTable.DataBoxes.Last();

            int rightmostWidthPosition = lastTextBox.Location.X + lastTextBox.Width;
            int leftIndent = (firstTextBox.Location.X + _calculateButton.Button.Width);

            int miscalculation = 1;

            int centerPosition = ((rightmostWidthPosition - leftIndent) / 2) - miscalculation;
            int widthPosition = centerPosition + firstTextBox.Location.X;

            var indentFromTextBoxes = 15;
            int heightPosition = lastTextBox.Location.Y + lastTextBox.Size.Height + indentFromTextBoxes;

            return new Point(widthPosition, heightPosition);
        }

        private void ConfigureSizes()
        {
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(600, 220);
        }

        internal static TextBox CreateTextBox(Point location)
        {
            return new TextBox()
            {
                Location = location,
                Size = _defaultTextBoxSize
            };
        }
        
        internal static Label CreateLabel(string content)
        {
            return new Label()
            {
                AutoSize = true,
                Name = content,
                Text = content
            };
        }

        internal static Button CreateButton(string content)
        {
            return new Button()
            {
                AutoSizeMode = AutoSizeMode.GrowAndShrink,
                AutoSize = true,
                Text = content,
                UseVisualStyleBackColor = true
            };
        }
    }
}
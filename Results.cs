﻿using System.Text;

namespace TrapezeCalculator
{
    public class Results
    {
        private readonly Label _outputLabel;
        public Results(Point labelPosition, out Control outputControl)
        {
            _outputLabel = CalculatorForm.CreateLabel(string.Empty);
            _outputLabel.Location = labelPosition;

            outputControl = _outputLabel;
        }

        public void ShowResults(IReadOnlyList<PointF> coordinates)
        {
            _outputLabel.BackColor = default;

            float area = MathF.Round(coordinates.GetArea(), 2);
            float perimeter = MathF.Round(coordinates.GetPerimeter(), 2);

            _outputLabel.Text = $"Площадь фигуры: {area}\n" +
                                $"Периметр: {perimeter}\n\n" +
                                "Длины сторон:\n" +
                                coordinates.GetSidesLengthText();
        }

        public void ShowError(string errorText)
        {
            _outputLabel.BackColor = Color.Crimson;
            _outputLabel.ForeColor = Color.White;
            _outputLabel.BorderStyle = BorderStyle.FixedSingle;

            _outputLabel.Text = errorText;
        }
    }
}

﻿namespace TrapezeCalculator
{
    public class CoordinatesTable2D : DataTable
    {
        private readonly Color _errorColor = Color.FromArgb(255, 239, 237);

        public CoordinatesTable2D(string[] verticalLabelNames, Point startPosition) :
            base(verticalLabelNames, new[] { "X", "Y" }, startPosition)
        {
            foreach (TextBox textBox in DataBoxes)
            {
                textBox.TextChanged += TextBoxOnTextChanged;
            }
        }

        public IReadOnlyList<PointF>? Coordinates
        {
            get
            {
                var isErrorOccurred = false;

                var coordinates = new List<PointF>();
                int coordinatesCount = DataBoxes.Count / 2;

                for (var i = 0; i < coordinatesCount; i++)
                {
                    TextBox widthPositionData = DataBoxes[i];
                    TextBox heightPositionData = DataBoxes[i + coordinatesCount];

                    float widthCoordinate = GetPositionValue(widthPositionData);
                    float heightCoordinate = GetPositionValue(heightPositionData);

                    if (float.IsNaN(widthCoordinate) || float.IsNaN(heightCoordinate))
                    {
                        isErrorOccurred = true;
                        continue;
                    }

                    coordinates.Add(new PointF(widthCoordinate, heightCoordinate));
                }

                if (isErrorOccurred)
                {
                    return null;
                }

                return coordinates;
            }
        }

        private float GetPositionValue(TextBox positionData)
        {
            if (IsEnteredDataValid(positionData, out float position))
            {
                return position;
            }
            
            positionData.BackColor = _errorColor;
            return float.NaN;
        }

        private static bool IsEnteredDataValid(Control textBox, out float result)
        {
            return float.TryParse(textBox.Text, out result);
        }
        
        private void TextBoxOnTextChanged(object? sender, EventArgs e)
        {
            var textBox = sender as TextBox;

            if (string.IsNullOrEmpty(textBox!.Text) || IsEnteredDataValid(textBox, out _))
            {
                textBox.BackColor = default;
                return;
            }

            textBox.BackColor = _errorColor;
        }
    }
}
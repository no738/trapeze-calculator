﻿namespace TrapezeCalculator
{
    public class DataTable : IDisposable
    {
        private readonly List<Label> _horizontalLabels = new();
        private readonly int _horizontalLabelsStep = 80;

        private readonly List<Label> _verticalLabels = new();
        private readonly int _verticalLabelsStep = 30;

        private readonly List<TextBox> _dataBoxes = new();

        public DataTable(string[] verticalLabelNames, string[] horizontalLabelNames, Point startPosition)
        {
            var padding = 50;

            CreateVerticalLabels(verticalLabelNames, new Point(startPosition.X, padding));
            CreateHorizontalLabels(horizontalLabelNames, new Point(padding, startPosition.Y));
            CreateTextBoxes();
        }

        ~DataTable()
        {
            ReleaseUnmanagedResources();
        }
        
        public IReadOnlyList<Control> Controls
        {
            get
            {
                var controls = new List<Control>();

                controls.AddRange(_dataBoxes);
                controls.AddRange(_verticalLabels);
                controls.AddRange(_horizontalLabels);

                return controls;
            }
        }
        public IReadOnlyList<TextBox> DataBoxes => _dataBoxes;

        private void CreateVerticalLabels(string[] labelNames, Point startPoint)
        {
            Point nextLabelLocation = startPoint;

            foreach (string labelName in labelNames)
            {
                Label label = CalculatorForm.CreateLabel(labelName);
                label.Location = nextLabelLocation;

                _verticalLabels.Add(label);

                nextLabelLocation.Y += _verticalLabelsStep;
            }
        }

        private void CreateHorizontalLabels(string[] labelNames, Point startPoint)
        {
            Point nextLabelLocation = startPoint;

            foreach (string labelName in labelNames)
            {
                Label label = CalculatorForm.CreateLabel(labelName);
                label.Location = nextLabelLocation;

                _horizontalLabels.Add(label);

                nextLabelLocation.X += _horizontalLabelsStep;
            }
        }

        private void CreateTextBoxes()
        {
            foreach (Label label in _horizontalLabels)
            {
                for (var i = 1; i <= _verticalLabels.Count; i++)
                {
                    int widthPosition = label.Location.X;
                    int heightPosition = label.Location.Y + _verticalLabelsStep * i;

                    var location = new Point(widthPosition, heightPosition);
                    _dataBoxes.Add(CalculatorForm.CreateTextBox(location));
                }
            }
        }

        private void ReleaseUnmanagedResources()
        {
            foreach (Control control in Controls)
            {
                control.Dispose();
            }
        }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }
    }
}
